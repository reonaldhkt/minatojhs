// gulp.js 設定
var
  // モジュール
  gulp = require('gulp'), //gulp
  runSequence = require('run-sequence'), //直列処理のためのやつ
  del = require('del'), //ファイル削除
  newer = require('gulp-newer'), //更新されたやつだけ通す
  plumber = require('gulp-plumber'), //watch中にエラー起きても続ける
  notify = require('gulp-notify'), //通知
  imagemin = require('gulp-imagemin'), //画像圧縮
  htmlclean = require('gulp-htmlclean'), //HTML圧縮
  babel = require('gulp-babel'), //ES6をES5に変換
  uglify = require('gulp-uglify'), //Javascrupt圧縮
  cleancss = require('gulp-clean-css'), //CSS圧縮
  // base64 = require('gulp-base64'), //base64エンコード
  // pug = require('gulp-pug'), //Pug
  sass = require('gulp-sass'), //SASS
  concat = require('gulp-concat'), // concat
  autoprefixer = require('gulp-autoprefixer'), //CSSのプレフィックスつけるやつ
  pdfThumbnail = require('gulp-pdf-thumbnail'), // PDF寒生る
  webserver = require('gulp-webserver') //webサーバ立ち上げ

  // developmentモードかどうか
  devBuild = (process.env.NODE_ENV !== 'production'),

  // フォルダーの設定
  folder = {
    source: 'source/',
    build: 'build/',
    minify: 'minify/'
  }
;

//ビルドフォルダ内の全てのファイルを削除
gulp.task('all-delete', function(cb){
  return del([folder.build + '**/**/*'], cb);
});

//ミニファイフォルダ内の全てのファイルを削除
gulp.task('all-delete-minify', function(cb){
  return del([folder.minify + '**/**/*'], cb);
});

//ソースフォルダ内の全てのファイルを構造をそのままにコピー
// ########## 必ず最後に実行 ###########
gulp.task('all-copy', function(){
  return gulp.src([folder.source + '**/**/*', '!' + folder.source + '**/**/*.pug', '!' + folder.source + '**/**/*.scss'], { base: folder.source })
    .pipe(newer(folder.build))
    .pipe(gulp.dest(folder.build));
});

//ビルドフォルダ内の全てのファイルを構造をそのままにコピー
// ########## 必ず最後に実行 ###########
gulp.task('all-copy-minify', function(){
  return gulp.src(folder.build + '**/**/*', { base: folder.build })
    .pipe(newer(folder.minify))
    .pipe(gulp.dest(folder.minify));
});

// 画像圧縮タスク
gulp.task('image-minify', function() {
  return gulp.src([folder.build + '**/**/*.png', folder.build + '**/**/*.jpg'], { base: folder.build })
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>") //<-
    }))
    .pipe(newer(folder.minify))
    .pipe(imagemin({ optimizationLevel: 5 }).on('error', function(e){
      console.log(e);
    }))
    .pipe(gulp.dest(folder.minify));
});

// HTML圧縮タスク
gulp.task('html-minify', function(){
  return gulp.src(folder.build + '**/**/*.html', { base: folder.build })
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>") //<-
    }))
    .pipe(newer(folder.minify))
    .pipe(htmlclean().on('error', function(e){
      console.log(e);
    }))
    .pipe(gulp.dest(folder.minify));
});

// Javascript圧縮タスク
gulp.task('js-minify', function(){
  return gulp.src([folder.build + '**/**/*.js', '!' + folder.build + '**/**/_*.js'], { base: folder.build })
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>") //<-
    }))
    .pipe(newer(folder.minify))
    .pipe(babel())
    .pipe(uglify().on('error', function(e){
      console.log(e);
    }))
    .pipe(gulp.dest(folder.minify));
});

// CSS圧縮タスク
gulp.task('css-minify', function(){
  return gulp.src(folder.build + '**/**/*.css', { base: folder.build })
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>") //<-
    }))
    .pipe(newer(folder.minify))
    .pipe(cleancss().on('error', function(e){
      console.log(e);
    }))
    .pipe(gulp.dest(folder.minify));
});

gulp.task('concat', function(){
  return gulp.src([folder.build + '**/**/concat_*.css'], { base: folder.build })
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>") //<-
    }))
    .pipe(concat('concat.css'))
    .pipe(gulp.dest(folder.build));
});

// SASSコンパイルタスク
gulp.task('sass', function(){
  return gulp.src([folder.source + '**/**/*.sass', folder.source + '**/**/*.scss', '!' + folder.source + '**/**/_*.*'], { base: folder.source })
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>") //<-
    }))
    .pipe(newer(folder.build))
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(autoprefixer({
      // ☆IEは11以上、Androidは4.4以上
      // その他は最新2バージョンで必要なベンダープレフィックスを付与する設定
      browsers: ["last 2 versions", "ie >= 11", "Android >= 4"],
      cascade: false
    }))
    .pipe(gulp.dest(folder.build));
});

// PDFサムネイル生成タスク
gulp.task('pdf-thumbnail', function(){
  return gulp.src(folder.source + '**/**/*.pdf', { base: folder.source })
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>") //<-
    }))
    .pipe(newer(folder.build))
    .pipe(pdfThumbnail())
    .pipe(gulp.dest(folder.build));
});


gulp.task('webserver', function () {
    gulp.src(folder.build) // 公開したい静的ファイルを配置したディレクトリを指定する
        .pipe(webserver({
            host: 'localhost',
            port: 8001,
            livereload: false
        }));
});

//Pugコンパイルタスク
// gulp.task('pug', function(){
//   return gulp.src(folder.source + '**/**/*.pug', { base: folder.source })
//     .pipe(plumber({
//       errorHandler: notify.onError("Error: <%= error.message %>") //<-
//     }))
//     .pipe(newer(folder.build))
//     .pipe(pug({ pretty: true }))
//     .pipe(gulp.dest(folder.build));
// });

// 画像, HTML, CSS, Javascriptを圧縮
gulp.task('minify', function(){
  console.log('####### ALL-MINIFYが実行されました #######');
  runSequence('image-minify', 'html-minify', 'css-minify', 'js-minify', 'all-copy-minify');
});

//ビルド
gulp.task('build', function(){
  console.log('####### BUILDが実行されました #######');
  runSequence('sass', 'all-copy');
});

//再ビルド
gulp.task('rebuild', function(){
  console.log('####### BUILDが実行されました #######');
  runSequence('all-delete',　'sass', 'all-copy');
});

gulp.task('netlify-deploy', function(){
  runSequence('sass', 'all-copy', 'image-minify', 'html-minify', 'css-minify', 'all-copy-minify');
});

//全てのファイルを再ビルド
//gulp.task('all-rebuild', ['all-delete', 'build'], function(){});

// watch sourceフォルダ内のファイルが更新されたら all-minify を実行
gulp.task('default', function(){
  runSequence('all-delete',　'sass', 'all-copy', 'watch', 'webserver');

});

gulp.task('watch', function(){
  gulp.watch(folder.source + '**/**/*', ['build']);
});
