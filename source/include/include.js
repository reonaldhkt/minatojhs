let htmlEscape = function(str){
  if (!str) return '';
  return str.replace(/[<>&"'`]/g, (match) => {
    const escape = {
      '<': '&lt;',
      '>': '&gt;',
      '&': '&amp;',
      '"': '&quot;',
      "'": '&#39;',
      '`': '&#x60;'
    };
    return escape[match];
  });
}

let afterInclude = function(){ // 外部HTMLの読み込み後に実行する関数
  // $(function(){ // 読み込み完了イベントのコールバック
  //   $('.links-link').on('click touchend', function(){
  //     location.href = $(this).find('a').attr('href');
  //   });
  //   $('.navi-link').on('click touchend', function(){
  //     location.href = $(this).find('a').attr('href');
  //   });
  //   $('.externalLinks-link').on('click touchend', function(){
  //     location.href = $(this).find('.link-link').find('a').attr('href');
  //   });
  // });
  // ############################### aタグで囲む様にしたので不要に！ ########################################
}


// 読み込む外部htmlファイルのパスと、置き換え元の要素を代入
let includeList = [
  {
    anker: $('.anker_name_header'),
    file: './include/header.html'
  },
  {
    anker: $('.anker_name_banner'),
    file: './include/banner.html'
  },
  {
    anker: $('.anker_name_navi'),
    file: './include/navi.html'
  },
  {
    anker: $('.anker_name_sidebar'),
    file: './include/sidebar.html'
  },
  {
    anker: $('.anker_name_footer'),
    file: './include/footer.html'
  }
];

// 読み込み
let includeHTML = function(setting){ // HTML読むコミ関数を作る
  let loop = 0;
  let done = 0;
  while(loop < includeList.length){
    let anker = includeList[loop].anker;
    let file = includeList[loop].file;
    $.get(file, function(data){
      $(data).replaceAll(anker);
      if(done == includeList.length - 1){ // これが最後のコールバックだったら
        afterInclude();
        if(setting.doBannerExpand == true) $('.banner').addClass('banner_expanded_true'); // バナーを広がった状態にする(交渉と校名を表示)
        if(setting.doNaviHomeHide == true) $('.navi-link_to_home').remove(); // ナビの項目からホームを削除
      }
      done++;
    });

    loop++;
  }
}

// Google Fontを非同期読み込み。ファーストビューの高速化のためメ
$('head').append('<link href=\'https://fonts.googleapis.com/earlyaccess/sawarabigothic.css\' rel=\'stylesheet\' type=\'text/css\'>');
$('body').css('font-family', '\'Sawarabi Gothic\'');

if (window.ontouchstart !== null) { //タッチデバイスじゃないとき
  $('body').niceScroll(); //NiceScrollを適用　スクロールが滑らかに！
  // タッチデバイスだとブラウザで慣性スクロールがデフォルトで付いて来るからつけない！
}
