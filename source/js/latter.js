pdfjsLib.workerSrc = "./js/_pdf.worker.js"; // workerのリンクを指定

// 追加先とPDFファイルのリンクからサムネイルが書かれたcanvasを追加先に追加する関数を定義
let renderPDFThumbnail = function(selector, url){ // SELECTORはcanvasタグが追加される対象のjQueryオブジェクトを、URLは絶対パスかlatter.htmlからの相対パス

  let target = selector.eq(0); // 対象を複数指定された時のために、最初の1個だけ含まれたjQueryオブジェクトを作る

  // PDFを非同期でダウンロード
  pdfjsLib.getDocument(url)
    .then(function(pdf) {
      return pdf.getPage(1); // 1ページ目を取得(サムネイル用に)
    })
    .then(function(page) {
      // ズームレベル。大きければ高画質に。サムネイルなのですごく小さくした
      var scale = 0.2;
      // ページを元にビューポートを取得
      var viewport = page.getViewport(scale);
      // canvas要素を生成
      let canvasObject = $('<canvas></canvas>');
      canvasObject.attr('width',  viewport.width.toString());
      canvasObject.attr('height', viewport.height.toString());
       // pdfのサムネイルが横長だったらcanvasにlandscapeクラスを付与。じゃなかったらportraitを付与
      viewport.width > viewport.height ? canvasObject.addClass('landscape') : canvasObject.addClass('portrait');
      target.append(canvasObject); // 対象に追加
      // 対象に追加されたcanvasタグを取得
      let addedCanvasObject = target.find('canvas');
      // Fetch canvas' 2d context
      var context = addedCanvasObject.get(0).getContext('2d'); // 生のjavascriptDOMを取得するためにget(0)した
      // Prepare object needed by render method
      var renderContext = {
        canvasContext: context,
        viewport: viewport
      };
      // サムネイルをcanvasに描画
      page.render(renderContext);
    });
}

includeHTML();
// renderPDFThumbnail($('.piece-thumbnail').eq(0), './pdf/grade2/april.pdf');
// renderPDFThumbnail($('.piece-thumbnail').eq(1), './pdf/grade2/may.pdf');











// // JSONファイルからお便りの表のHTMLを生成
// $.getJSON('./data/latter.json', function(data){
//   console.log(data);
//   // console.log(createLatterContainerHTML(data[0].latter[0]));
//   let latterHTML = '';
//   let loop = 0;
//   while(loop < data.length){ // data配列の長さだけループ(グループの数だけループ)
//     let thisGroupArray = data[loop];
//     let categoryInnerHTML = '';
//     let loop2 = 0;
//     while(loop2 < thisGroupArray.latter.length){ // data[loop].latter配列の長さだけループ(このグループに含まれるの手紙の数だけループ)
//       let thisLatterArray = thisGroupArray.latter[loop2];
//       let additionalClass; // data.unclickableによってクラスを追加するかどうか決める
//       thisLatterArray.unclickable == true ? additionalClass = 'content-piece_unclickable_true' : additionalClass = '';
//       let pieceHTML = `
//         <div class="content-piece ${htmlEscape(additionalClass)}">
//           <div class="piece-thumbnail flex"></div>
//           <div class="piece-title flex">
//             ${htmlEscape(thisLatterArray.name)}
//           </div>
//           <a href="${htmlEscape(thisLatterArray.url)}"></a>
//         </div>
//       `; // 生成されたお便りの一部分のHTMLコードを代入
//       categoryInnerHTML = categoryInnerHTML + pieceHTML; // HTMLコードをpiecesHTMLの末尾に追加
//       loop2++;
//     }
//     // console.log(piecesHTML);
//     let categoryHTML = `
//       <div class="latter-category flex">
//         <div class="category-title flex"><h3>${htmlEscape(thisGroupArray.categoryName)}</h3></div>
//         <div class="category-content">
//           ${categoryInnerHTML}
//         </div>
//       </div>
//     `; // 生成されたカテゴリのHTMLコードを代入
//     latterHTML = latterHTML + categoryHTML;
//     loop++;
//   }
//   console.log(latterHTML);
//   $('.group_name_latter').find('.group-content').append($(latterHTML));
// });

// JSONファイルからお便りの表のHTMLを生成
$.getJSON('./data/latter.json', function(data){
  console.log(data);
  // console.log(createLatterContainerHTML(data[0].latter[0]));
  let latterHTML = '';

  let loop = 0;
  while(loop < data.length){ // data配列の長さだけループ(グループの数だけループ)

    let thisGroupArray = data[loop];
    $('.group_name_latter').find('.group-content').append($(`
      <div class="latter-category flex">
        <div class="category-title flex"><h3>${htmlEscape(thisGroupArray.categoryName)}</h3></div>
        <div class="category-content">
        </div>
      </div>
    `));

    let loop2 = 0;
    while(loop2 < thisGroupArray.latter.length){ // data[loop].latter配列の長さだけループ(このグループに含まれるの手紙の数だけループ)
      let thisLatterArray = thisGroupArray.latter[loop2];
      let lastCategoryContent = $('.group_name_latter').children('.group-content').children(':last').children('.category-content');
      let additionalClass; // data.unclickableによってクラスを追加するかどうか決める
      thisLatterArray.unclickable == true ? additionalClass = 'content-piece_unclickable_true' : additionalClass = '';
      // 一番最後のlatter-categoryのcategory-contentの中の末尾に追加
      lastCategoryContent.append($(`
        <div class="content-piece ${htmlEscape(additionalClass)}">
          <div class="piece-thumbnail flex"></div>
          <div class="piece-title flex">
            ${htmlEscape(thisLatterArray.name)}
          </div>
          <a href="${htmlEscape(thisLatterArray.url)}"></a>
        </div>
      `));
      console.log(lastCategoryContent.children(':last'));
      // renderPDFThumbnail(lastCategoryContent.children(':last').children('.piece-thumbnail'), thisLatterArray.url);
      lastCategoryContent.children(':last').children('.piece-thumbnail').append($(`
        <img src="${htmlEscape(thisLatterArray.thumbnail)}" class="landscape"></img>
      `));
      loop2++;
    }

    loop++;
  }

});





// let loop2 = 0;
// while(loop2 < thisGroupArray.latter.length){ // data[loop].latter配列の長さだけループ(このグループに含まれるの手紙の数だけループ)
//   let thisLatterArray = thisGroupArray.latter[loop2];
//   let additionalClass; // data.unclickableによってクラスを追加するかどうか決める
//   thisLatterArray.unclickable == true ? additionalClass = 'content-piece_unclickable_true' : additionalClass = '';
//   let pieceHTML = `
//     <div class="content-piece ${htmlEscape(additionalClass)}">
//       <div class="piece-thumbnail flex"></div>
//       <div class="piece-title flex">
//         ${htmlEscape(thisLatterArray.name)}
//       </div>
//       <a href="${htmlEscape(thisLatterArray.url)}"></a>
//     </div>
//   `; // 生成されたお便りの一部分のHTMLコードを代入
//   categoryInnerHTML = categoryInnerHTML + pieceHTML; // HTMLコードをpiecesHTMLの末尾に追加
//   loop2++;
// }
