includeHTML({
  doBannerExpand: true,
  doNaviHomeHide: true
});

// ####### スケジュール部分の生成(jsonから) #######

let createWhenPieceHTML = function(date, hasTilde){ // チルダ（波ダッシュ）をつけるときはtildeに left' か 'right' を指定
  let tildeLetter = '<span>〜</span>';
  let dayName = [ // 曜日番号に対応する曜日名
    '日', '月', '火', '水', '木', '金', '土'
  ];
  let customColorClass = [ // 曜日番号に対応する背景色のつけるCSSが含まれるクラス名
    'piece-day_color_red', // 日曜日
    '', '', '', '', '',    // それ以外(月火水木金)
    'piece-day_color_blue' // 土曜日
  ];
  let dateDate = String(date.getMonth() + 1) + '/' + String(date.getDate()); // なぜか月は0から数え始めるので1をたす
  let dateDayId = date.getDay(); // 曜日を取得
  // let dateDayName = dayName[dateDayId]; // 曜日を対応する文字に変換
  if(hasTilde === 'left' ) dateDate = tildeLetter + dateDate; // チルダをつける
  if(hasTilde === 'right') dateDate = dateDate + tildeLetter;
  let whenPieceHTML = `
<div class="when-piece flex">
  <div class="piece-day ${customColorClass[dateDayId]} flex"><h3>${dayName[dateDayId]}</h3></div>
  <div class="piece-date flex"><h3>${dateDate}</h3></div>
</div>
  `;
  return whenPieceHTML;
}

$.get('./data/schedule.json', function(data){ // jsonを読み込む
  let groupContent = $('.group_name_schedule').find('.group-content');
  schedule = data;
  console.log(data);
  // データの並べ替え
  {
    let loop = 0;
    while(loop < schedule.length){
      let target = schedule[loop].when; // array.sortは元の配列を書き換えてしまうので、コピーした配列をソートしてから元の配列に代入する
      // console.log('before: ', target);
      { // 日付の範囲選択の配列の要素の順番を早く来る順に
        let loop2 = 0;
        while(loop2 < target.length){ // 日付配列の要素の数だけループ
          if(Array.isArray(target[loop2]) == true){ // loop2番目の配列の要素が配列だったら
            target[loop2].sort(function(a, b){ // ソートする　この配列の要素は全て文字列
              // 比較関数の決まり
              // この関数が0未満を返す時、aをbより小さい添字（配列の要素番号）に入れ替える。
              // この関数が0を返す時、お互いに変更しない
              // この関数が0より大きい値を返す時、aをbより大きい添字（配列の要素番号）に入れ替える。
              let milliSecondA = new Date(a).getTime(); // 比較するために1970/1/1から経過したmsに変換
              let milliSecondB = new Date(b).getTime();
              if(milliSecondA < milliSecondB) return -1; // 1970/1/1からの経過時間が少ない順に入れ替えする
              if(milliSecondA > milliSecondB) return  1;
              return 0; // どちらでもない（等しい）だったらそのまま
            });
          }
          loop2++;
        }
      }
      target.sort(function(a, b){ // ソートする　この配列の要素は文字列と配列が混ざってる
        let milliSecondA, milliSecondB;
         // 比較する要素が配列だったら、0番目の要素を元にDateオブジェクトを作成（比較する要素が日付の範囲選択の配列だったら、始まる日付を元にDateオブジェクトを作成）
         // じゃなかったら、その要素を元に作成
        Array.isArray(a) == true ? milliSecondA = new Date(a[0]).getTime() : milliSecondA = new Date(a).getTime();
        Array.isArray(b) == true ? milliSecondB = new Date(b[0]).getTime() : milliSecondB = new Date(b).getTime();
        if(milliSecondA < milliSecondB) return -1; // 1970/1/1からの経過時間が少ない順に入れ替えする
        if(milliSecondA > milliSecondB) return  1;
        return 0; // どちらでもない（等しい）だったらそのまま
      });
      // console.log('after: ', target);
    schedule[loop].when = target; // ソートしたもの代入
    loop++;
    }
    schedule.sort(function(a, b){ // 始まる日付順にならべかえる(早い順)
      Array.isArray(a.when[0]) == true ? milliSecondA = new Date(a.when[0][0]).getTime() : milliSecondA = new Date(a.when[0]).getTime();
      Array.isArray(b.when[0]) == true ? milliSecondB = new Date(b.when[0][0]).getTime() : milliSecondB = new Date(b.when[0]).getTime();
      if(milliSecondA < milliSecondB) return -1; // 1970/1/1からの経過時間が少ない順に入れ替えする
      if(milliSecondA > milliSecondB) return  1;
    });
    console.log(schedule);
  }
  {
    let loop = 0;
    while(loop < schedule.length){ // スケジュールの予定の数だけ繰り返す
      let whenInnerHTML = '';
      let loop2 = 0;
      // jsonファイルのデータを元に、日付部分のHTMLをwhenInnerHTMLに格納
      while(loop2 < schedule[loop].when.length){ // スケジュールの予定の日付が格納された配列の長さだけ繰り返す
        if(Array.isArray(schedule[loop].when[loop2]) == false){ // その配列のloop2ばんめが配列じゃなかったら
          let when = new Date(schedule[loop].when[loop2]); // 格納されていた日付の文字列を元にDateオブジェクトを作成
          whenInnerHTML = whenInnerHTML + createWhenPieceHTML(when); // その日付を元にHTMLコードを生成してwhenInnerHTMLの背後に追加
        }
        if(Array.isArray(schedule[loop].when[loop2]) == true){ // その配列のloop2ばんめが配列だったら
          let tildePosition = ['right', 'left'];
          let loop3 = 0;
          while(loop3 < 2){ // 配列の時は日付が範囲で指定されているということなので、最初と最後の日付を読み込むために、2回繰り返す(正しく書かれたwhen配列の中にある配列の長さは2にしかならない)
            let when = new Date(schedule[loop].when[loop2][loop3]); // 格納されていた日付の文字列を元にDateオブジェクトを作成
            whenInnerHTML = whenInnerHTML + createWhenPieceHTML(when, tildePosition[loop3]); // その日付を元にHTMLコードを生成してwhenInnerHTMLの背後に追加
            loop3++;
          }
        }
        loop2++;
      }
      // 格納完了
      // やること(todo)部分のHTMLを作成
      let todoInnerHTML = schedule[loop].todo; // 今はそんな凝ったことしないからそのまま入れる
      // 作成完了
      // whenInnerHTMLとtodoInnerHTMLを元にHTMLコードを作成
      let scheduleContainerHTML = `
<div class="schedule-container flex">
  <div class="container-when flex">
    ${whenInnerHTML}
  </div>
  <div class="container-todo flex">
    ${todoInnerHTML}
  </div>
</div>
      `;
      // console.log(scheduleContainerHTML);
      groupContent.append(scheduleContainerHTML); // 作ったHTMLコードをスケジュールに追加
      loop++;
    }
  }
  // $('.piece-day').eq(2).addClass('piece-day_color_red');
  // $('.piece-day').eq(3).addClass('piece-day_color_blue');
});

// let array = [3,1,7,4,8,2,9,0,6,2,5];
// array.sort(function(a, b){
//   console.log(a, b);
//   return a - b;
// });
